= upside - design and implementation of an open-hardware/open-source UPS =

On 2018-02-08 I published a blog rant titled
http://esr.ibiblio.org/?p=7839[UPSes suck and need to be disrupted]
complaining about the deficiencies of crappy Uninterruptible Power
Supply designs that perform poorly and pile hidden costs on their
users in order to minimize vendors' NRE and BOM costs.  I suggested
that this whole product category needs to be disrupted by an
open-hardware design that addresses the many deficiencies of existing
hardware. UPSes are not complicated devices; there is no good excuse
for the state of the commmercial art to be as inadequate as it is.

The response on my blog and G+ was intense, almost overwhelming. It
seems many UPS users are unhappy with what the vendors are pushing.

This project is an attempt to do something about that.  Our goal is to
design a high-quality UPS that can be built from off-the-shelf parts in
any reasonably well-equipped makerspace or home electronics shop.  Our
final deliverable should be PCB designs, a full parts list, assembly
instructions, and full manuals for the hardware and software.

We welcome contributors: people with interest in UPSes who have
expertise in battery technology, power-switching electronics, writing
device-control firmware, relevant standards such as USB and the DMTF
battery-management profile.

We also welcome participation from established UPS and electronics
vendors.  We know that consumer electronics is a cutthroat low-margin
business in which it's tough to support a real R&D team or make
possibly-risky product bets. Help us, and then let us help you!

To get a handle on the state of the project it is probably best to
begin by browsing the https://gitlab.com/esr/upside/wikis/home[wiki].
Then you should browse the design documents in the repository.

You might be especially interested in the
https://gitlab.com/esr/upside/wikis/task-list[task list] showing completed
and pending work.

You should also read the
https://gitlab.com/esr/upside/blob/master/process.adoc[process
document] to learn how to contribute effectively.


