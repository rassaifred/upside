// This file is Copyright (c) 2018 by the UPSide project.
// SPDX-License-Identifier: BSD-2-clause
//
// Smart Battery Data 1.1 message interpretation
//
// Specification at http://sbs-forum.org/specs/sbdat110.pdf

package sbd


import (
	"fmt"
	"sort"
	"strings"
)

var sbsMessageMap map[string]int
var sbsBatteryModeBits map[string]int
var sbsBatteryStatusBits map[string]int

func init() {
	// Host to smart battery message types
	// Response comes back tagged with the request code. May be a NAK.
	sbsMessageMap = map[string]int{
		"ManufacturerAccess": 0x00,
		"RemainingCapacityAlarm": 0x01,
		"RemainingTimeAlarm": 0x02,
		"BatteryMode": 0x03,
		"AtRate": 0x04,
		"AtRateTimeToFull": 0x05,
		"AtRateTimeToEmpty": 0x06,
		"AtRateOK": 0x07,
		"Temperature": 0x08,
		"Voltage": 0x09,
		"Current": 0x0a,
		"AverageCurrent": 0x0b,
		"MaxError": 0x0c,
		"RelativeStateOfCharge": 0x0d,
		"AbsoluteStateOfCharge": 0x0e,
		"RemainingCapacity": 0x0f,
		"FullChargeCapacity": 0x10,
		"RunTimeToEmpty": 0x11,
		"AverageTimeToEmpty": 0x12,
		"AverageTimeToFull": 0x13,
		"BatteryStatus": 0x16,
		"CycleCount": 0x17,
		"DesignCapacity": 0x18,
		"DesignVoltage": 0x19,
		"SpecificationInfo": 0x1a,
		"ManufactureDate": 0x1b,
		"SerialNumber": 0x1c,
		"ManufacturerName": 0x20,
		"DeviceName": 0x21,
		"DeviceChemistry": 0x22,
		"ManufacturerData": 0x23,
	}

	sbsBatteryModeBits = map[string]int{
		"InternalChargeController": 0,
		"PrimaryBatterySupport": 1,
		"ConditionFlag": 7,
		"ChargeControllerEnabled": 8,
		"PrimaryBattery": 9,
		"AlarmMode": 13,
		"ChargerMode": 14,
		"CapacityMode": 15,
	}

	sbsBatteryStatusBits = map[string]int{
		"OverChargedAlarm": 0x8000,
		"TerminateChargeAlarm": 0x4000,
		"OverTempAlarm": 0x1000,
		"TerminateDischargeAlarm": 0x0800,
		"Remaining_capacityAlarm": 0x0200,
		"RemainingTimeAlarm": 0x0100,
		"Initialized": 0x0080,
		"Discharging": 0x0040,
		"FullyCharged": 0x0020,
		"FullyDischarged": 0x0010,
	}
}

type SBDMessage struct {
	MessageType string
	UnsignedIntArg uint16
	SignedIntArg int16
	FlagArg bool
	BlockData []byte
}

type SBDContext struct {
	/* See SBD 4.4.1 */
	CapacityMode bool
	ChargerMode bool
	AlarmMode bool
	PrimaryBattery bool
	ChargeControllerEnabled bool
}

/*
 * SBDName - map from SBD operation code to function name
 */
func SBDName(msgcode int) string {
	for k, v := range sbsMessageMap {
		if v == msgcode {
			return k
		}
	}
	return ""
}

/*
 * SBDCode - map from SBD function name to operation code
 */
func SBDCode(msg string) int {
	v, ok := sbsMessageMap[msg]
	if ok {
		return v
	} else {
		return -1
	}
}

/*
 * SBDStatusName - map from SBD status bit to status name
 */
func SBDStatusName(stcode int) string {
	for k, v := range sbsBatteryStatusBits {
		if v == stcode {
			return k
		}
	}
	return ""
}

/*
 * SBDStatusCode - map from SBD status name to status code
 */
func SBDStatusCode(msg string) int {
	v, ok := sbsBatteryStatusBits[msg]
	if ok {
		return v
	} else {
		return -1
	}
}

/*
 * GetBits - extract a field from a 16-bit word, render as int
 */
func GetBits(n uint16, start uint, end uint) uint16 {
	n &= (0xFFFF >> (16 - end))
	n >>= start
	return n
}

func SetBits(n *uint16, val uint16, start uint, end uint) {
	val &= (0xFFFF >> (16 - end))
	*n |= (val << start)
}

/*
 * Marshal - emit text representation of SBD message
 */
func (ctx *SBDContext) Marshal(msg *SBDMessage) string {
	var args string
	var tokens []string
	switch msg.MessageType {
	case "RemainingCapacityAlarm":
		if ctx.CapacityMode {
			args = fmt.Sprintf("%d 10mWh", msg.UnsignedIntArg)
		} else {
			args = fmt.Sprintf("%d mAh", msg.UnsignedIntArg)
		}
	case "RemainingTimeAlarm":
		args = fmt.Sprintf("%d minutes", msg.UnsignedIntArg)
	case "BatteryMode":
		for k, v := range sbsBatteryModeBits {
			// Assumes host byte order
			if msg.UnsignedIntArg & (1 << uint(v)) != 0 {
				tokens = append(tokens, k)
			}
		}
		sort.Strings(tokens)
		args = strings.Join(tokens, " ")
	case "AtRate":
		args = fmt.Sprintf("%d", msg.SignedIntArg)		
	case "AtRateTimeToFull":
		args = fmt.Sprintf("%d minutes", msg.UnsignedIntArg)
	case "AtRateTimeToEmpty":
		args = fmt.Sprintf("%d minutes", msg.UnsignedIntArg)
	case "AtRateOK":
		args = fmt.Sprintf("%t", msg.FlagArg)
	case "Temperature":
		args = fmt.Sprintf("%d dK", msg.UnsignedIntArg)
	case "Voltage":
		args = fmt.Sprintf("%d mV", msg.UnsignedIntArg)
	case "Current":
		args = fmt.Sprintf("%d mA", msg.SignedIntArg)
	case "AverageCurrent":
		args = fmt.Sprintf("%d mA", msg.SignedIntArg)		
	case "MaxError":
		args = fmt.Sprintf("%d %%", msg.UnsignedIntArg)
	case "RelativeStateOfCharge":
		args = fmt.Sprintf("%d %%", msg.UnsignedIntArg)
	case "AbsoluteStateOfCharge":
		args = fmt.Sprintf("%d %%", msg.UnsignedIntArg)
	case "RemainingCapacity":
		if ctx.CapacityMode {
			args = fmt.Sprintf("%d 10mWh", msg.UnsignedIntArg)
		} else {
			args = fmt.Sprintf("%d mAh", msg.UnsignedIntArg)
		}
	case "FullChargeCapacity":
		if ctx.CapacityMode {
			args = fmt.Sprintf("%d 10mWh", msg.UnsignedIntArg)
		} else {
			args = fmt.Sprintf("%d mAh", msg.UnsignedIntArg)
		}		
	case "RunTimeToEmpty":
		args = fmt.Sprintf("%d minutes", msg.UnsignedIntArg)		
	case "AverageTimeToEmpty":
		args = fmt.Sprintf("%d minutes", msg.UnsignedIntArg)
	case "AverageTimeToFull":
		args = fmt.Sprintf("%d minutes", msg.UnsignedIntArg)		
	case "BatteryStatus":
		args = ""
		for k, v := range sbsBatteryStatusBits {
			// Assumes host byte order
			if msg.UnsignedIntArg & uint16(v) != 0 {
				tokens = append(tokens, k)
			}
		}		
		sort.Strings(tokens)
		args = strings.Join(tokens, " ")
	case "CycleCount":
		args = fmt.Sprintf("%d cycles", msg.UnsignedIntArg)
	case "DesignCapacity":
		if ctx.CapacityMode {
			args = fmt.Sprintf("%d 10mWh", msg.UnsignedIntArg)
		} else {
			args = fmt.Sprintf("%d mAh", msg.UnsignedIntArg)
		}				
	case "DesignVoltage":
		args = fmt.Sprintf("%d mV", msg.UnsignedIntArg)		
	case "SpecificationInfo":
		/* See 5.1.25 for interpretation of these fields */
		revision := GetBits(msg.UnsignedIntArg, 0, 1)
		version := GetBits(msg.UnsignedIntArg, 4, 7)
		vscale := GetBits(msg.UnsignedIntArg, 8, 11)
		ipscale := GetBits(msg.UnsignedIntArg, 12, 15)
		args = fmt.Sprintf("revision %d version %d vscale %d ipscale %d",
			revision, version, vscale, ipscale)
	case "ManufactureDate":
		day := GetBits(msg.UnsignedIntArg, 0, 4)
		month := GetBits(msg.UnsignedIntArg, 5, 8)
		year := GetBits(msg.UnsignedIntArg, 9, 15)
		args = fmt.Sprintf("%04d-%02d-%02d", 1980+year, month, day)
	case "SerialNumber":
		args = fmt.Sprintf("%d", msg.UnsignedIntArg)
	case "ManufacturerName":
		args = fmt.Sprintf("%q", msg.BlockData)
	case "DeviceName":
		args = fmt.Sprintf("%q", msg.BlockData)
	case "DeviceChemistry":
		args = fmt.Sprintf("%q", msg.BlockData)
	case "ManufacturerData":
		args = fmt.Sprintf("%X", msg.BlockData)
	default:
		panic(fmt.Sprintf("unknown message type in SBSData block %v", msg))
	}
	if len(args) == 0 {
		return msg.MessageType
	} else {
		return msg.MessageType + " " + args
	}
}

/*
 * Unmarshal - read in text representation of SBD message, return message
 */
func (ctx *SBDContext) Unmarshal(text string) *SBDMessage {
	msg := new(SBDMessage)
	fields := strings.Split(text, " ")
	msg.MessageType = fields[0]
	var args string
	if len(fields) == 1 {
		args = ""
	} else {
		args = text[len(fields[0]) + 1:len(text)]
	}
	var n int
	var err error
	switch fields[0] {
	case "RemainingCapacityAlarm":
		if ctx.CapacityMode {
			n, err = fmt.Sscanf(args, "%d 10mWh", &msg.UnsignedIntArg)
		} else {
			n, err = fmt.Sscanf(args, "%d mAh", &msg.UnsignedIntArg)
		}
	case "RemainingTimeAlarm":
		n, err = fmt.Sscanf(args, "%d minutes", &msg.UnsignedIntArg)
	case "BatteryMode":
		for i := 1; i < len(fields); i++ {
			// Assumes host byte order
			msg.UnsignedIntArg |= (1 << uint(sbsBatteryModeBits[fields[i]]))
		}
		n = 1	/* this can have no args, that's OK */
	case "AtRate":
		n, err = fmt.Sscanf(args, "%d", &msg.SignedIntArg)		
	case "AtRateTimeToFull":
		n, err = fmt.Sscanf(args, "%d minutes", &msg.UnsignedIntArg)
	case "AtRateTimeToEmpty":
		n, err = fmt.Sscanf(args, "%d minutes", &msg.UnsignedIntArg)
	case "AtRateOK":
		n, err = fmt.Sscanf(args, "%t", &msg.FlagArg)
	case "Temperature":
		n, err = fmt.Sscanf(args, "%d dK", &msg.UnsignedIntArg)
	case "Voltage":
		n, err = fmt.Sscanf(args, "%d mV", &msg.UnsignedIntArg)
	case "Current":
		n, err = fmt.Sscanf(args, "%d mA", &msg.SignedIntArg)
	case "AverageCurrent":
		n, err = fmt.Sscanf(args, "%d mA", &msg.SignedIntArg)		
	case "MaxError":
		n, err = fmt.Sscanf(args, "%d %%", &msg.UnsignedIntArg)
	case "RelativeStateOfCharge":
		n, err = fmt.Sscanf(args, "%d %%", &msg.UnsignedIntArg)
	case "AbsoluteStateOfCharge":
		n, err = fmt.Sscanf(args, "%d %%", &msg.UnsignedIntArg)
	case "RemainingCapacity":
		if ctx.CapacityMode {
			n, err = fmt.Sscanf(args, "%d 10mWh", &msg.UnsignedIntArg)
		} else {
			n, err = fmt.Sscanf(args, "%d mAh", &msg.UnsignedIntArg)
		}
	case "FullChargeCapacity":
		if ctx.CapacityMode {
			n, err = fmt.Sscanf(args, "%d 10mWh", &msg.UnsignedIntArg)
		} else {
			n, err = fmt.Sscanf(args, "%d mAh", &msg.UnsignedIntArg)
		}		
	case "RunTimeToEmpty":
		n, err = fmt.Sscanf(args, "%d minutes", &msg.UnsignedIntArg)		
	case "AverageTimeToEmpty":
		n, err = fmt.Sscanf(args, "%d minutes", &msg.UnsignedIntArg)
	case "AverageTimeToFull":
		n, err = fmt.Sscanf(args, "%d minutes", &msg.UnsignedIntArg)		
	case "BatteryStatus":
		for n = 1; n < len(fields); n++ {
			msg.UnsignedIntArg |= uint16(sbsBatteryStatusBits[fields[n]])
		}		
	case "CycleCount":
		n, err = fmt.Sscanf(args, "%d cycles", &msg.UnsignedIntArg)
	case "DesignCapacity":
		if ctx.CapacityMode {
			n, err = fmt.Sscanf(args, "%d 10mWh", &msg.UnsignedIntArg)
		} else {
			n, err = fmt.Sscanf(args, "%d mAh", &msg.UnsignedIntArg)
		}				
	case "DesignVoltage":
		n, err = fmt.Sscanf(args, "%d mV", &msg.UnsignedIntArg)		
	case "SpecificationInfo":
		var revision uint16
		var version uint16
		var vscale uint16
		var ipscale uint16
		/* See 5.1.25 for interpretation of these fields */
		n, err = fmt.Sscanf(args, "revision %d version %d vscale %d ipscale %d",
			&revision, &version, &vscale, &ipscale)
		if n < 4 {
			panic("ill-formed specification info")
		}
		SetBits(&msg.UnsignedIntArg, revision, 0, 1)
		SetBits(&msg.UnsignedIntArg, version, 4, 7)
		SetBits(&msg.UnsignedIntArg, vscale, 8, 11)
		SetBits(&msg.UnsignedIntArg, ipscale, 12, 15)
	case "ManufactureDate":
		var year uint16
		var month uint16
		var day uint16
		n, err = fmt.Sscanf(args, "%04d-%02d-%02d", &year, &month, &day)
		if n < 3 {
			panic("ill-formed manufacturing date")
		}
		year -= 1980
		SetBits(&msg.UnsignedIntArg, day, 0, 4)
		SetBits(&msg.UnsignedIntArg, month, 5, 8)
		SetBits(&msg.UnsignedIntArg, year, 9, 15)
	case "SerialNumber":
		n, err = fmt.Sscanf(args, "%d", &msg.UnsignedIntArg)
	case "ManufacturerName":
		n, err = fmt.Sscanf(args, "%q", &msg.BlockData)
	case "DeviceName":
		n, err = fmt.Sscanf(args, "%q", &msg.BlockData)		
	case "DeviceChemistry":
		n, err = fmt.Sscanf(args, "%q", &msg.BlockData)
	case "ManufacturerData":
		n, err = fmt.Sscanf(args, "%x", &msg.BlockData)
	default:
		panic("unknown message type in SBSData representation")
	}
	if err != nil {
		panic(err)
	} else if n == 0 {
		panic("no argument found where expected")
	}
	return msg
}
	
// end

