// This file is Copyright (c) 2018 by the UPSide project.
// SPDX-License-Identifier: BSD-2-clause
//
// The single instance of this class provides an object that looks to the
// policy code like a power plane.  It's actually a sinilation jig - takes an
// event file on standard input and ships a report of actions taken by the
// policy logic to standard output. Use it to perform regression tests.

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sbd"
	"strconv"
	"strings"
	"time"

	"hardware"
)

var sbdc sbd.SBDContext

type SimulatorType struct {
	Reader *bufio.Reader
	exhausted bool
	sensors map[string]hardware.SensorType
	sensorSeq []string
}

func (s *SimulatorType) registerSensor(id string, t hardware.SensorType) {
	if s.sensors[id] == hardware.INVALID {
		s.sensorSeq = append(s.sensorSeq, id)
	}
	s.sensors[id] = t
}

func (s *SimulatorType) Init() {
	s.Reader = bufio.NewReader(os.Stdin)
	s.sensors = make(map[string]hardware.SensorType)
	s.sensorSeq = make([]string, 0)
}

func (s *SimulatorType) Continue() bool {
	return !s.exhausted
}

func (s *SimulatorType) SensorList() []string {
	return s.sensorSeq
}

func popField(text string) (string, string) {
	fld := strings.Split(text, " ")[0]
	text = text[len(fld) + 1:len(text)]
	return fld, text
}

func (s *SimulatorType) PollSensor(take *hardware.SensorTake) bool {
	// Parse the next line of simulated sensor data
	take.Type = hardware.INVALID

	line, isP, err := s.Reader.ReadLine()
	if err == io.EOF {
		s.exhausted = true
		return false
	} else if err != nil {
		panic(err)
	} else if isP {
		panic("line too long")
	}
	text := string(line)

	// Echo to the check log
	fmt.Printf("%s\n", text)

	// One field has nonstandard syntax
	if strings.HasSuffix(text, " wait") {
		return false
	}

	// Ignore comments
	if text[0] == '#' {
		return true
	}
	
	var timestamp string
	timestamp, text = popField(text)
	var class string
	class, text = popField(text)
	var id string
	id, text = popField(text)

	take.ID = id
	tt, _ := strconv.Atoi(timestamp)
	take.Timestamp = time.Unix(int64(tt), 0) // fake date near Unix epoch

	switch class {
	case "ac":
		take.Type = hardware.AC
		i, err := fmt.Sscanf(text, "%d amps", &take.Amps)
		if err != nil || i != 1 {
			fmt.Fprintf(os.Stderr, "malformed ac event line")
			return true
		}
		s.registerSensor(id, hardware.AC)
	case "dc":
		take.Type = hardware.DC
		i, err := fmt.Sscanf(text, "%d volts %d amps", &take.Volts, &take.Amps)
		if err != nil || i != 2 {
			fmt.Fprintf(os.Stderr, "malformed dc event line")
			return true
		}
		s.registerSensor(id, hardware.DC)
	case "bms":
		take.Type = hardware.BMS
		take.Bms = sbdc.Unmarshal(text)
		s.registerSensor(id, hardware.BMS)
	case "btn":
		var bi uint
		var bv bool
		take.Type = hardware.BTN
		s.registerSensor(id, hardware.BTN)
		fmt.Sscanf(take.ID, "btn%d %t", &bi)
		fmt.Sscanf(text, "%t", &bv)
		take.ButtonSet(bi, bv)
	case "host":
		take.Type = hardware.HOST
		take.HostShutdownAck = true
	}

	return true
}

func (s *SimulatorType) SensorClass(id string) hardware.SensorType {
	return s.sensors[id]
}

func (s *SimulatorType) Display(st string) {
	fmt.Print("--------------------\n")
	fmt.Print(st)
	fmt.Print("\n--------------------\n")
}

func (s *SimulatorType) ShutdownHost() {
	fmt.Printf("shutdown host\n")
}

func (s *SimulatorType) OutletControl(id string, enable bool) {
	if enable {
		fmt.Printf("outlet %v on\n", id)
	} else {
		fmt.Printf("outlet %v off\n", id)
	}
}

func (s *SimulatorType) Buzz(freq int, dur time.Duration) {
	//fmt.Printf("buzz %v hz %v\n", freq, dur)
}

func (s *SimulatorType) Wait(t time.Duration) {
	fmt.Printf("wait %v\n", t)
}

func (s *SimulatorType) ShutdownUPS() {
	fmt.Printf("shutdown UPS\n")
	os.Exit(0)
}

var Simulator SimulatorType

// end



