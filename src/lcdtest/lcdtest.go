package main

import (
	"log"
	"time"

	"golang.org/x/exp/io/i2c"

	"hardware"
	"hd44780"
)

func main() {
	i2c, err := i2c.Open(&i2c.Devfs{Dev: hardware.Configuration.DisplayDevice}, hardware.Configuration.DisplayAddress)
	if err != nil { log.Fatal(err) }
	// Free I2C connection on exit
	defer i2c.Close()
	// Construct lcd-device connected via I2C connection
	lcd, err := hd44780.NewLcd(i2c, hd44780.LCD_16x2)
	if err != nil { log.Fatal(err) }
	// Make it obvious what state we're in
	err = lcd.BacklightOff()
	if err != nil { log.Fatal(err) }
	time.Sleep(time.Second)
	err = lcd.BacklightOn()
	if err != nil { log.Fatal(err) }
	// Put text on the lcd-display
	err = lcd.Display("Hello\nWorld")
	if err != nil { log.Fatal(err) }
	// Wait 5 secs
	time.Sleep(5 * time.Second)
	// Turn off the backlight and exit
	err = lcd.BacklightOff()
	if err != nil { log.Fatal(err) }
}
